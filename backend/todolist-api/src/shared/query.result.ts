export class QueryResult<T> {
  data: T[];
  totalOfRegister: number;
  pageNumber: number;

  constructor(data: T[], totalOfRegister: number, pageNumber: number) {
    this.data = data;
    this.totalOfRegister = totalOfRegister;
    this.pageNumber = pageNumber;
  }
}
