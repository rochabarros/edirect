export class QueryParameters {
  fields: string;
  sortFields: string;
  sortType: string;
  limit: number;
  offset: number;
  filter: string;
  value: string;

  constructor(
    fields: string,
    sortFields: string,
    sortType: string,
    limit: number,
    offset: number,
    filter: string,
    value: string,
  ) {
    this.fields = fields;
    this.sortFields = sortFields;
    this.sortType = sortType;
    this.limit = limit;
    this.offset = offset;
    this.filter = filter;
    this.value = value;
  }
}
