import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(pUsername: string, pPass: string): Promise<any> {
    const user = await this.usersService.getUserByUsername(pUsername);
    if (user && this.usersService.isPasswordValid(pPass, user.password)) {
      if (!this.usersService.isUserActivo(user)) {
        return null;
      }
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.email };
    const userDatabase = await this.usersService.getUserByUsername(
      user.username,
    );
    return {
      user: userDatabase,
      accessToken: this.jwtService.sign(payload),
    };
  }
}
