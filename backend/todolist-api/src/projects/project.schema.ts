import * as mongoose from 'mongoose';

export const ProjectSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    active: {
      type: Boolean,
    },
    creationDate: {
      type: Date,
    },
    finishDate: {
      type: Date,
    },
  },
  { collection: 'projects' },
);
