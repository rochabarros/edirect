import {
  Injectable,
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Project } from './project';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProjectDto } from './project.dto';
import { QueryParameters } from '../shared/query.parameters';
import { QueryResult } from '../shared/query.result';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectModel('Project') private readonly projectModel: Model<Project>,
  ) {}

  async getProjectById(id: string): Promise<Project> {
    if (!id) {
      throw new BadRequestException('Id not found');
    }
    let project: Project;
    try {
      project = await this.projectModel.findById(id);
    } catch (error) {
      throw new InternalServerErrorException(
        `Error trying to get project by id ${id}`,
      );
    }
    if (!project) {
      throw new NotFoundException(`Project with id ${id} not found`);
    }
    return project;
  }

  async createProject(projectDto: ProjectDto): Promise<Project> {
    try {
      const projectCreated = new this.projectModel(projectDto);
      return await projectCreated.save();
    } catch (error) {
      throw new InternalServerErrorException('Error trying to create project');
    }
  }

  async updateProject(id: string, projectDto: ProjectDto): Promise<Project> {
    const project = await this.getProjectById(id);
    try {
      await this.projectModel.findByIdAndUpdate(id, projectDto);
      return await this.getProjectById(id);
    } catch (error) {
      throw new InternalServerErrorException(
        `Error trying to update project by id ${id}`,
      );
    }
  }

  async deleteProject(id: string): Promise<string> {
    try {
      const project = await this.getProjectById(id);
      await this.projectModel.findByIdAndRemove(id);
      return `Project ${project.name} deleted`;
    } catch (err) {
      throw new InternalServerErrorException(
        `Error trying to delete project by id ${id}`,
      );
    }
  }

  async findProjects(
    queryParameters: QueryParameters,
  ): Promise<QueryResult<Project>> {
    try {
      const filtros = {};
      if (queryParameters.value) {
        filtros[queryParameters.filter] = new RegExp(
          '.*' + queryParameters.value + '.*',
          'i',
        );
      }
      const total = await this.projectModel.count(filtros);
      const filedsArray = queryParameters.fields.split(',');
      const projetos = await this.projectModel.find(filtros, filedsArray, {
        limit: queryParameters.limit,
        skip: queryParameters.offset,
      });
      return new QueryResult(projetos, total, 0);
    } catch (err) {
      throw new InternalServerErrorException(`Error trying to find projects`);
    }
  }
}
