import {
  Controller,
  Post,
  Put,
  Delete,
  Get,
  Param,
  Body,
  Query,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectDto } from './project.dto';
import { Project } from './project';
import { QueryResult } from '../shared/query.result';
import { QueryParameters } from '../shared/query.parameters';

@Controller('projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService) {}

  @Get(':id')
  async getProjectById(@Param('id') id: string): Promise<Project> {
    return await this.projectsService.getProjectById(id);
  }

  @Post()
  async createProject(@Body() projectDto: ProjectDto): Promise<Project> {
    return await this.projectsService.createProject(projectDto);
  }

  @Put(':id')
  async updateProject(
    @Param('id') id: string,
    @Body() projectDto: ProjectDto,
  ): Promise<Project> {
    return await this.projectsService.updateProject(id, projectDto);
  }

  @Delete(':id')
  async delteProject(@Param('id') id: string): Promise<string> {
    return await this.projectsService.deleteProject(id);
  }

  @Get()
  async findProjects(
    @Query() query: QueryParameters,
  ): Promise<QueryResult<Project>> {
    return await this.projectsService.findProjects(query);
  }
}
