import { Document } from 'mongoose';

export interface Project extends Document {
  name: string;
  descriptiopn: string;
  active: boolean;
  creationDate: Date;
  finishDate: Date;
}
