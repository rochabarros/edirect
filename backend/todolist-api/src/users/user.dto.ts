export class UserDto {
  email: string;
  name: string;
  username: string;
  password: string;
  active: boolean;
}
