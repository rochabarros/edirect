import {
  Injectable,
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { User } from './user';
import { UserDto } from './user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getUserById(id: string): Promise<User> {
    if (!id) {
      throw new BadRequestException('Id not found');
    }
    let user: User;
    try {
      user = await this.userModel.findById(id);
    } catch (error) {
      throw new InternalServerErrorException(
        `Error trying to get user by id ${id}`,
      );
    }
    if (!user) {
      throw new NotFoundException(`User with id ${id} not found`);
    }
    return user;
  }

  async getUserByUsername(pUsername: string): Promise<User> {
    if (!pUsername) {
      throw new BadRequestException('Username not found');
    }
    let user: User;
    try {
      user = await this.userModel.findOne({ username: pUsername });
    } catch (error) {
      throw new InternalServerErrorException(
        `Error trying to get user by username ${pUsername}`,
      );
    }
    if (!user) {
      throw new NotFoundException(`User with username ${pUsername} not found`);
    }
    return user;
  }

  async createUser(userDto: UserDto): Promise<User> {
    try {
      this.getUserByUsername(userDto.username);
      const cryptPassword = this.cryptPassword(userDto.password);
      userDto.password = cryptPassword;
      const userCreated = new this.userModel(userDto);
      await userCreated.save();
      return await this.getUserByUsername(userDto.username);
    } catch (error) {
      throw new InternalServerErrorException('Error trying to create user');
    }
  }

  cryptPassword(valor: string): string {
    return bcrypt.hashSync(valor, bcrypt.genSaltSync(10));
  }

  isPasswordValid(valor: string, valorCrypt: string): boolean {
    return bcrypt.compareSync(valor, valorCrypt);
  }

  async isUserActivo(user: User): Promise<boolean> {
    return user && user.active;
  }
}
