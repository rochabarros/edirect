import { User } from "../users/user";
import { Project } from "../projects/project";
import { Document } from 'mongoose';

export interface Task extends Document {
  name: string;
  descriptiopn: string;
  statu: boolean;
  creationDate: Date;
  finishDate: Date;
  user: User;
  project: Project;
}
