import { Injectable, BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task } from './task';
import { Model } from 'mongoose';
import { QueryParameters } from 'src/shared/query.parameters';
import { QueryResult } from 'src/shared/query.result';
import { TaskDto } from './task.dto';

@Injectable()
export class TasksService {

    constructor(
        @InjectModel('Task') private readonly taskModel: Model<Task>,
    ) { }

    async getTaskById(id: string): Promise<Task> {
        if (!id) {
            throw new BadRequestException('Id not found');
        }
        let task: Task;
        try {
            task = await this.taskModel.findById(id);
        } catch (error) {
            throw new InternalServerErrorException(
                `Error trying to get task by id ${id}`,
            );
        }
        if (!task) {
            throw new NotFoundException(`Task with id ${id} not found`);
        }
        return task;
    }

    async createTask(taskDto: TaskDto): Promise<Task> {
        try {
            const taskCreated = new this.taskModel(taskDto);
            return await taskCreated.save();
        } catch (error) {
            throw new InternalServerErrorException('Error trying to create task');
        }
    }

    async updateTask(id: string, taskDto: TaskDto): Promise<Task> {
        const task = await this.getTaskById(id);
        try {
            await this.taskModel.findByIdAndUpdate(id, taskDto);
            return await this.getTaskById(id);
        } catch (error) {
            throw new InternalServerErrorException(
                `Error trying to update task by id ${id}`,
            );
        }
    }

    async deleteTask(id: string): Promise<string> {
        try {
            const task = await this.getTaskById(id);
            await this.taskModel.findByIdAndRemove(id);
            return `Task ${task.name} deleted`;
        } catch (err) {
            throw new InternalServerErrorException(
                `Error trying to delete task by id ${id}`,
            );
        }
    }

    async findTasks(
        queryParameters: QueryParameters,
    ): Promise<QueryResult<Task>> {
        try {
            const filtros = {};
            if (queryParameters.value) {
                filtros[queryParameters.filter] = new RegExp(
                    '.*' + queryParameters.value + '.*',
                    'i',
                );
            }
            const total = await this.taskModel.count(filtros);
            const filedsArray = queryParameters.fields.split(',');
            const projetos = await this.taskModel.find(filtros, filedsArray, {
                limit: queryParameters.limit,
                skip: queryParameters.offset,
            });
            return new QueryResult(projetos, total, 0);
        } catch (err) {
            throw new InternalServerErrorException(`Error trying to find tasks`);
        }
    }
}
