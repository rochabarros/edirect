import { Controller, Post, Put, Param, Body, Delete, Get, Query } from '@nestjs/common';
import { TaskDto } from './task.dto';
import { Task } from './task';
import { TasksService } from './tasks.service';
import { QueryParameters } from '../shared/query.parameters';
import { QueryResult } from '../shared/query.result';

@Controller('tasks')
export class TasksController {

    constructor(private tasksService: TasksService) { }

    @Get(':id')
    async getTaskById(@Param('id') id: string): Promise<Task> {
        return await this.tasksService.getTaskById(id);
    }

    @Post()
    async createTask(@Body() taskDto: TaskDto): Promise<Task> {
        return await this.tasksService.createTask(taskDto);
    }

    @Put(':id')
    async updateTask(
        @Param('id') id: string,
        @Body() taskDto: TaskDto,
    ): Promise<Task> {
        return await this.tasksService.updateTask(id, taskDto);
    }

    @Delete(':id')
    async delteTask(@Param('id') id: string): Promise<string> {
        return await this.tasksService.deleteTask(id);
    }

    @Get()
    async findTasks(
        @Query() query: QueryParameters,
    ): Promise<QueryResult<Task>> {
        return await this.tasksService.findTasks(query);
    }
}
