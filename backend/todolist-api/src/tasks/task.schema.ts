import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    status: {
      type: String,
    },
    creationDate: {
      type: Date,
    },
    finishDate: {
      type: Date,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId, ref: 'User'
    },
    project: {
      type: mongoose.Schema.Types.ObjectId, ref: 'Project'
    }
  },
  { collection: 'tasks' },
);
