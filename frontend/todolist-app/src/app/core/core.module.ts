import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesService, AuthService, LocalStorageService } from '@core/services';


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    MessagesService,
    AuthService,
    LocalStorageService
  ],
})
export class CoreModule { }
