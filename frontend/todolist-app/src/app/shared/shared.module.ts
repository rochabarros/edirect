import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BlankLayoutComponent,
  FullLayoutComponent,
  IconLoaderComponent,
  BreadcrumbComponent,
  SpinnerComponent,
  FormFieldComponent,
} from '@app/shared';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    BlankLayoutComponent,
    FullLayoutComponent,
    IconLoaderComponent,
    BreadcrumbComponent,
    SpinnerComponent,
    FormFieldComponent
  ],
  exports: [
    BlankLayoutComponent,
    FullLayoutComponent,
    IconLoaderComponent,
    BreadcrumbComponent,
    SpinnerComponent,
    FormFieldComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
