import {
  Component,
  Input,
  OnChanges,
  OnInit,
  AfterViewInit
} from '@angular/core';

import { MessagesService } from '@core/services/messages.service'

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit, OnChanges, AfterViewInit {

  @Input()
  public control: any;
  @Input()
  public controlName: string;
  @Input()
  public apiErrorType?: string;
  // TODO : How to pass apiServiceUrl
  @Input()
  public apiServiceUrl?: string;

  public errorObject: Object = {
    required: '{{field}} is required',
    email: 'Please enter valid email'
  };
  public errorKeys: string[];
  public apiErrorMessage: string;

  constructor(private messagesService: MessagesService) {
    messagesService.errors$.subscribe(
      (errors: ErrorModel.ErrorMessageObject[]) => {
        errors
          .filter(
            e => e.type === this.apiErrorType
            //  && e.serviceUrl == this.apiServiceUrl
          )
          .map(e => {
            this.apiErrorMessage = e.error;
          });
      }
    );
  }

  ngOnInit() { }

  ngOnChanges() {
    this.errorKeys = Object.keys(this.errorObject);
  }

  ngAfterViewInit() {
    this.control.valueChanges.subscribe(() => {
      this.apiErrorMessage = '';
    });
  }

  formateError(errorMessage: string, errorObj: any): string {
    const types = ['min', 'max', 'requiredLength'];

    types.forEach(type => {
      if (!!errorObj[type]) {
        errorMessage = errorMessage.replace(/{{value}}/g, errorObj[type]);
      }
    });
    return errorMessage.replace(/{{field}}/g, this.controlName);
  }

  hasError() {
    return (
      (this.control.errors && this.control.touched) || this.apiErrorMessage
    );
  }
}
