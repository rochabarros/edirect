import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-icon-loader',
  templateUrl: './icon-loader.component.html'
})
export class IconLoaderComponent implements OnInit {

  @Input()
  isLoading: boolean;

  @Input()
  label: string;

  constructor() {}

  ngOnInit() {}

}
