import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from '@shared/layouts/full-layout/full-layout.component';
import { BlankLayoutComponent } from '@shared/layouts/blank-layout/blank-layout.component';


const AppRoutes: Routes = [
  // {
  //   path: '',
  //   component: FullLayoutComponent,
  //   children: [
  //     {
  //       path: '',
  //       redirectTo: '/home',
  //       pathMatch: 'full'
  //     },
  //     {
  //       path: 'home',
  //       loadChildren: () =>
  //         import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  //     }
  //   ]
  // },
  {
    path: '',
    component: BlankLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./auth/auth.module').then(
            m => m.AuthModule
          )
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
